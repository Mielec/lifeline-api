package pl.mielecmichal.lifeline.api.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pl.mielecmichal.lifeline.api.entities.PeriodEntity;

public interface PeriodsRepository extends CrudRepository<PeriodEntity, Long>{

}
