package pl.mielecmichal.lifeline.api.application;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.orm.jpa.EntityScan;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import pl.mielecmichal.lifeline.api.controllers.periods.PeriodsController;
import pl.mielecmichal.lifeline.api.entities.PeriodEntity;
import pl.mielecmichal.lifeline.api.repositories.PeriodsRepository;
import pl.mielecmichal.lifeline.api.services.PeriodsService;

@EntityScan(basePackageClasses = PeriodEntity.class)
@EnableJpaRepositories(basePackageClasses = PeriodsRepository.class)
@SpringBootApplication(scanBasePackageClasses = {PeriodsController.class, PeriodsService.class})
public class Application {

    public static void main(String[] args)
    {
        ApplicationContext context = SpringApplication.run(Application.class, args);
    }

}

