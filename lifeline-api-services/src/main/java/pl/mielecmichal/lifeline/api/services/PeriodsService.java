package pl.mielecmichal.lifeline.api.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.mielecmichal.lifeline.api.entities.PeriodEntity;
import pl.mielecmichal.lifeline.api.repositories.PeriodsRepository;

import java.time.LocalDate;
import java.time.Month;
import java.util.Arrays;
import java.util.List;

@Service
public class PeriodsService {

    private final PeriodsRepository periodsRepository;

    @Autowired
    public PeriodsService(PeriodsRepository periodsRepository) {
        this.periodsRepository = periodsRepository;

        List<PeriodEntity> periods = Arrays.asList(
                new PeriodEntity("Zerówka", LocalDate.of(2000, Month.SEPTEMBER, 1), LocalDate.of(2001, Month.JUNE, 30), 5, 8),
                new PeriodEntity("Podstawówka", LocalDate.of(2000, Month.SEPTEMBER, 1), LocalDate.of(2001, Month.JUNE, 30), 5, 8),
                new PeriodEntity("Gimbaza", LocalDate.of(2000, Month.SEPTEMBER, 1), LocalDate.of(2001, Month.JUNE, 30), 5, 8)
        );

        periodsRepository.save(periods);
    }

    public Iterable<PeriodEntity> findAll() {
        return periodsRepository.findAll();
    }

}
