package pl.mielecmichal.lifeline.api.controllers.periods;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import pl.mielecmichal.lifeline.api.entities.PeriodEntity;
import pl.mielecmichal.lifeline.api.repositories.PeriodsRepository;
import pl.mielecmichal.lifeline.api.services.PeriodsService;

import java.time.LocalDate;
import java.time.Month;
import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping(value = "/periods")
public class PeriodsController {

    private final PeriodsService periodsService;

    @Autowired
    public PeriodsController(PeriodsService periodsService) {
        this.periodsService = periodsService;
    }

    @RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public Iterable<PeriodEntity> preparePeriods() {
        return periodsService.findAll();
    }


}
