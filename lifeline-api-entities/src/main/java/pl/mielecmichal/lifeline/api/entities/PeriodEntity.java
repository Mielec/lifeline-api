package pl.mielecmichal.lifeline.api.entities;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name = "periods")
public class PeriodEntity {

    @Id
    @GeneratedValue
    private Long id;

    @Column(nullable = false)
    private String name;

    @Column(nullable = false)
    private LocalDate startDate;

    @Column(nullable = false)
    private LocalDate endDate;

    @Column(nullable = false)
    private Integer daysPerWeek;

    @Column(nullable = false)
    private Integer hoursPerWeek;

    public PeriodEntity() {
        this.name = null;
        this.startDate = null;
        this.endDate = null;
        this.daysPerWeek = null;
        this.hoursPerWeek = null;
    }

    public PeriodEntity(String name, LocalDate startDate, LocalDate endDate, Integer daysPerWeek, Integer hoursPerWeek) {
        this.name = name;
        this.startDate = startDate;
        this.endDate = endDate;
        this.daysPerWeek = daysPerWeek;
        this.hoursPerWeek = hoursPerWeek;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }

    public Integer getDaysPerWeek() {
        return daysPerWeek;
    }

    public void setDaysPerWeek(Integer daysPerWeek) {
        this.daysPerWeek = daysPerWeek;
    }

    public Integer getHoursPerWeek() {
        return hoursPerWeek;
    }

    public void setHoursPerWeek(Integer hoursPerWeek) {
        this.hoursPerWeek = hoursPerWeek;
    }
}
